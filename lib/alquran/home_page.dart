import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:tiktok_ui/alquran/list_surat_page.dart';

class HomePageQuran extends StatefulWidget {
  const HomePageQuran({Key? key}) : super(key: key);

  @override
  State<HomePageQuran> createState() => _HomePageQuranState();
}

class _HomePageQuranState extends State<HomePageQuran> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Container(),
        actions: const [
          IconButton(
              onPressed: null,
              icon: Icon(
                LineIcons.facebookMessenger,
                size: 30,
              )),
          IconButton(
            onPressed: null,
            icon: CircleAvatar(
              radius: 15,
            ),
          )
        ],
        leading: IconButton(onPressed: null, icon: Icon(LineIcons.list)),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: [
            RefreshIndicator(
              onRefresh: () async {},
              child: ListView(
                children: [
                  const Text(
                    "Assalamualaikum",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.grey),
                  ),
                  const Text(
                    "Firman Prayoga",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                        fontSize: 18),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => ListSuratPage(),));
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Stack(
                        children: [
                          Container(
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topRight,
                                  end: Alignment.bottomLeft,
                                  colors: [
                                    Colors.pinkAccent.withOpacity(.6),
                                    Colors.blue.withOpacity(.9),
                                  ],
                                ),
                                borderRadius: BorderRadius.circular(10)),
                            padding: EdgeInsets.all(20),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Quran Completion",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "Last read : Al-Baqarah-1",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Stack(
                                        children: [
                                          Container(
                                            height: 5,
                                            width: double.maxFinite,
                                            decoration: BoxDecoration(
                                                color: Colors.white60,
                                                borderRadius:
                                                BorderRadius.circular(10)),
                                          ),
                                          Container(
                                            height: 5,
                                            width: 50,
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                BorderRadius.circular(10)),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Image.asset(
                                  'assets/quran.png',
                                  width: 80,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Row(
                    children: const [
                      Text(
                        "My Schedule",
                        style: TextStyle(
                            color: Colors.black87, fontWeight: FontWeight.bold),
                      ),
                      Spacer(),
                      TextButton(
                          onPressed: null,
                          child: Text(
                            "See All",
                            style: TextStyle(color: Colors.blue),
                          ))
                    ],
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        for (var i = 0; i < 10; i++)
                          Container(
                            width: MediaQuery.of(context).size.width - 200,
                            margin: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(.2),
                                      blurRadius: 10,
                                      spreadRadius: 1)
                                ]),
                            padding: EdgeInsets.all(15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  child: Text(
                                    "7 Feb 2023",
                                    style: TextStyle(
                                        fontSize: 10, color: Colors.green),
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.green.shade50,
                                      borderRadius: BorderRadius.circular(10)),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Quran Juz 1",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Al-Fatiha - Al-Baqarah",
                                  style: TextStyle(color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    CircleAvatar(
                                      backgroundColor: Colors.blue,
                                      radius: 13,
                                      child: Icon(
                                        Icons.book,
                                        size: 15,
                                        color: Colors.white,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text("Quran Lovers")
                                  ],
                                )
                              ],
                            ),
                          )
                      ],
                    ),
                  ),
                  Row(
                    children: const [
                      Text(
                        "Community",
                        style: TextStyle(
                            color: Colors.black87, fontWeight: FontWeight.bold),
                      ),
                      Spacer(),
                      TextButton(
                          onPressed: null,
                          child: Text(
                            "See All",
                            style: TextStyle(color: Colors.blue),
                          ))
                    ],
                  ),
                  ListView.builder(
                    itemCount: 10,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(.2),
                                  blurRadius: 10,
                                  spreadRadius: 1)
                            ]),
                        padding: EdgeInsets.all(20),
                        margin: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Container(
                              child: Icon(
                                CupertinoIcons.book_fill,
                                color: Colors.white,
                              ),
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(10)),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  Text(
                                    "Quran Lovers",
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "100 Members",
                                    style: TextStyle(color: Colors.grey),
                                  )
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                              ),
                            ),
                            IconButton(
                                onPressed: null,
                                icon: Icon(LineIcons.chevronCircleRight))
                          ],
                        ),
                      );
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        currentIndex: 0,
        selectedItemColor: Colors.blueAccent,
        unselectedItemColor: Colors.black54,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(LineIcons.home), label: ""),
          BottomNavigationBarItem(icon: Icon(LineIcons.book), label: ""),
          BottomNavigationBarItem(icon: Icon(LineIcons.thLarge), label: ""),
          BottomNavigationBarItem(icon: Icon(LineIcons.users), label: ""),
          BottomNavigationBarItem(icon: Icon(LineIcons.bookmark), label: ""),
        ],
      ),
    );
  }
}
