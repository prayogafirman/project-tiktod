import 'package:flutter/material.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:quran/quran.dart' as quran;


class ListSuratPage extends StatefulWidget {
  const ListSuratPage({Key? key}) : super(key: key);

  @override
  State<ListSuratPage> createState() => _ListSuratPageState();
}

class _ListSuratPageState extends State<ListSuratPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        foregroundColor: Colors.black87,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Al-Baqarah"),
        leading: IconButton(
          icon: Icon(LineIcons.arrowLeft),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(onPressed: null, icon: Icon(LineIcons.horizontalEllipsis)),
          IconButton(onPressed: null, icon: Icon(LineIcons.bookmark)),
        ],
      ),
      body: Container(
        child:  ListView(
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
              child: Stack(
                children: [
                  Container(
                    width: double.maxFinite,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [
                            Colors.pinkAccent.withOpacity(.6),
                            Colors.blue.withOpacity(.9),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.all(20),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Al-Fatiha",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "The Opening",
                                style: TextStyle(color: Colors.white),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "7 Ayahs",
                                style: TextStyle(color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Image.asset(
                          'assets/quran.png',
                          width: 80,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            ListView.builder(
              itemCount: quran.getVerseCount(1),
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(.2),
                            blurRadius: 10,
                            spreadRadius: 1)
                      ]),
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(onPressed: null, icon: Icon(LineIcons.horizontalEllipsis)),
                          IconButton(onPressed: null, icon: Icon(LineIcons.play)),
                        ],
                      ),
                      Text(
                        quran.getVerse(1, index + 1, verseEndSymbol: true),
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 20
                        ),
                      ),
                    ],
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
