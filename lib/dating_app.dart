import 'dart:math';

import 'package:flutter/material.dart';
import 'package:gradient_borders/box_borders/gradient_box_border.dart';
import 'package:line_icons/line_icons.dart';
import 'package:swipeable_card_stack/swipeable_card_stack.dart';

class DatingApp extends StatefulWidget {
  const DatingApp({Key? key}) : super(key: key);

  @override
  State<DatingApp> createState() => _DatingAppState();
}

class _DatingAppState extends State<DatingApp> {
  SwipeableCardSectionController _cardController =
      SwipeableCardSectionController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Icon(
              Icons.location_on,
              color: Colors.blue,
            ),
            Text(
              " Bogor,Jawa Barat",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            )
          ],
        ),
        leading: IconButton(
          icon: Icon(
            Icons.list_outlined,
            color: Colors.black87,
          ),
          onPressed: null,
        ),
        actions: [
          Stack(
            children: const [
              IconButton(
                icon: Icon(
                  LineIcons.bell,
                  color: Colors.black87,
                ),
                onPressed: null,
              ),
              Positioned(
                child: CircleAvatar(
                  radius: 4,
                ),
                top: 15,
                right: 15,
              )
            ],
          )
        ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(20),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(.2),
                                blurRadius: 10,
                                blurStyle: BlurStyle.normal)
                          ]),
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextField(
                        decoration: InputDecoration(
                            icon: Icon(LineIcons.search),
                            border: InputBorder.none,
                            hintText: "Search for partner"),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    style: ButtonStyle(
                        padding: MaterialStatePropertyAll(EdgeInsets.all(10)),
                        shape: MaterialStatePropertyAll(StadiumBorder())),
                    child: Icon(Icons.list_outlined),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Column(
                    children: [
                      Stack(
                        children: [
                          Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                                border: const GradientBoxBorder(
                                  gradient: LinearGradient(
                                      colors: [Colors.blue, Colors.purple]),
                                  width: 6,
                                ),
                                borderRadius: BorderRadius.circular(160)),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: Image.network(
                                  "https://i.pravatar.cc/150?img=0"),
                            ),
                          ),
                          const Positioned(
                            child: const CircleAvatar(
                              radius: 10,
                              backgroundColor: Colors.black87,
                              child: Icon(
                                LineIcons.plus,
                                size: 12,
                              ),
                            ),
                            bottom: 5,
                            right: 5,
                          )
                        ],
                      ),
                      Text(
                        "Firman",
                        style: TextStyle(fontSize: 12),
                      )
                    ],
                  ),
                  for (var i = 1; i < 10; i++)
                    Column(
                      children: [
                        Container(
                          width: 70,
                          height: 70,
                          decoration: BoxDecoration(
                              border: const GradientBoxBorder(
                                gradient: LinearGradient(
                                    colors: [Colors.blue, Colors.purple]),
                                width: 6,
                              ),
                              borderRadius: BorderRadius.circular(160)),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Image.network(
                                "https://i.pravatar.cc/150?img=${i}"),
                          ),
                        ),
                        Text(
                          "Elsa",
                          style: TextStyle(fontSize: 12),
                        )
                      ],
                    ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            SwipeableCardsSection(
              cardController: _cardController,
              context: context,
              //add the first 3 cards (widgets)
              cardHeightBottomMul: 20,
              cardHeightMiddleMul: 20,
              items: [
                for (var i = 0; i < 5; i++) swipeCard(id: i),
              ],
              //Get card swipe event callbacks
              onCardSwiped: (dir, index, widget) {
                //Add the next card using _cardController
                _cardController.addItem(swipeCard(id: index + 1));

                //Take action on the swiped widget based on the direction of swipe
                //Return false to not animate cards
              },
              enableSwipeUp: true,
              enableSwipeDown: true,
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        elevation: 0,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: " "),
          BottomNavigationBarItem(icon: Icon(LineIcons.heart), label: " "),
          BottomNavigationBarItem(icon: Icon(LineIcons.rocketChat), label: " "),
          BottomNavigationBarItem(icon: Icon(LineIcons.user), label: " "),
        ],
      ),
    );
  }

  Widget swipeCard({required int id}) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(.2),
                blurRadius: 10,
                blurStyle: BlurStyle.normal)
          ],
          borderRadius: BorderRadius.circular(20)),
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: AspectRatio(
              aspectRatio: 1,
              child: ClipRRect(
                child: Image.network(
                  "https://i.pravatar.cc/550?img=${id}",
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Text(
                      "Firman Prayoga",
                      style: TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    Text(
                      "Bogor, Jawa Barat",
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
              ),
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.info,
                    color: Colors.blueAccent,
                  ))
            ],
          ),
          // Text("Tampan dan berani!",style: TextStyle(
          //     color: Colors.black87
          // ),),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: null,
                child: Icon(Icons.close),
                style: ButtonStyle(
                    padding: MaterialStatePropertyAll(EdgeInsets.all(0)),
                  shape: MaterialStatePropertyAll(CircleBorder())
                ),
              ),
              ElevatedButton(
                onPressed: null,
                child: Icon(Icons.favorite,color: Colors.white,),
                style: ButtonStyle(
                    padding: MaterialStatePropertyAll(EdgeInsets.all(0)),
                  shape: MaterialStatePropertyAll(
                    CircleBorder()
                  ),
                  backgroundColor: MaterialStatePropertyAll(Colors.redAccent)
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
