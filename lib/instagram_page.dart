import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class InstagramPage extends StatefulWidget {
  const InstagramPage({Key? key}) : super(key: key);

  @override
  State<InstagramPage> createState() => _InstagramPageState();
}

class _InstagramPageState extends State<InstagramPage> {
  int tabIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.black87,
        title: SizedBox(
          width: 100,
          child: Image.asset(
            "assets/logo.png",
            color: Colors.white,
          ),
        ),
        actions: [
          IconButton(onPressed: () {}, icon: Icon(LineIcons.plus)),
          IconButton(onPressed: () {}, icon: Icon(LineIcons.heart)),
          IconButton(onPressed: () {}, icon: Icon(LineIcons.facebookMessenger))
        ],
        elevation: 0,
      ),
      body: RefreshIndicator(
        onRefresh: ()async{
          
        },
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: [
            Container(
              decoration: BoxDecoration(color: Colors.black87),
              padding: EdgeInsets.only(bottom: 10),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Column(
                      children: [
                        Stack(
                          children: [
                            CircleAvatar(
                              radius: 30,
                              backgroundColor: Colors.white,
                              child: Padding(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: Image.network(
                                      "https://picsum.photos/200"),
                                ),
                                padding: EdgeInsets.all(2),
                              ),
                            ),
                            Positioned(
                              child: CircleAvatar(
                                radius: 10,
                                backgroundColor: Colors.lightBlue,
                                child: Icon(
                                  Icons.add,
                                  size: 12,
                                  color: Colors.white,
                                ),
                              ),
                              bottom: 0,
                              right: 0,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Cerita Anda",
                          style: TextStyle(color: Colors.white, fontSize: 11),
                        )
                      ],
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    for (var i = 0; i < 10; i++)
                      Row(
                        children: [
                          Column(
                            children: [
                              CircleAvatar(
                                radius: 30,
                                backgroundColor: Colors.white,
                                child: Padding(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: Image.network(
                                        "https://picsum.photos/id/${i}/200/200"),
                                  ),
                                  padding: EdgeInsets.all(2),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "lorem_",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 11),
                                textAlign: TextAlign.center,
                              )
                            ],
                          ),
                          SizedBox(
                            width: 5,
                          )
                        ],
                      )
                  ],
                ),
              ),
            ),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: 10,
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(color: Colors.black87),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: 20,
                              backgroundColor: Colors.white,
                              child: Padding(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: Image.network(
                                      "https://picsum.photos/id/${index}/200/200"),
                                ),
                                padding: EdgeInsets.all(2),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Firmanpraa_",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Spacer(),
                            Icon(
                              Icons.more_vert_sharp,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                      AspectRatio(
                        aspectRatio: 1,
                        child: Image.network(
                            "https://picsum.photos/id/${index + 100}/500/500",
                          fit: BoxFit.cover,
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(onPressed: null, icon: Icon(LineIcons.heart,color: Colors.white,)),
                          IconButton(onPressed: null, icon: Icon(LineIcons.facebookMessenger,color: Colors.white,)),
                          IconButton(onPressed: null, icon: Icon(Icons.send,color: Colors.white,)),
                          Spacer(),
                          IconButton(onPressed: null, icon: Icon(LineIcons.bookmark,color: Colors.white,))
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment  : CrossAxisAlignment.start,
                          children: [
                            Text("firmanpraa_",style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),),
                            SizedBox(
                              height: 5,
                            ),
                            Text("Lorem Ipsum adalah contoh teks atau dummy",style: TextStyle(
                              color: Colors.white
                            ),),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: 16,
                              backgroundColor: Colors.white,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: Image.network(
                                    "https://picsum.photos/id/${index}/200/200"),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(child: TextField(
                              decoration: InputDecoration(
                                hintText: "Tambahkan komentar...",
                                hintStyle: TextStyle(
                                  color: Colors.white70
                                ),
                                border: InputBorder.none
                              ),
                              style: TextStyle(
                                color: Colors.white
                              ),
                            )),

                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: tabIndex,
        onTap: (v) => setState(() {
          tabIndex = v;
        }),
        backgroundColor: Colors.black87,
        unselectedItemColor: Colors.white,
        selectedItemColor: Colors.white,
        items: [
          BottomNavigationBarItem(icon: Icon(LineIcons.home), label: " "),
          BottomNavigationBarItem(icon: Icon(LineIcons.search), label: " "),
          BottomNavigationBarItem(
              icon: Image.asset(
                'assets/Reels.png',
                color: Colors.white,
                height: 25,
              ),
              label: " "),
          BottomNavigationBarItem(
              icon: Icon(LineIcons.shoppingBag), label: " "),
          BottomNavigationBarItem(
              icon: CircleAvatar(
                radius: 16,
                child: CircleAvatar(
                  radius: 16,
                  backgroundColor: Colors.white,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.network(
                        "https://picsum.photos/id/0/200/200"),
                  ),
                ),
              ),
              label: " "),
        ],
      ),
    );
  }
}
