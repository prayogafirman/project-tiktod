import 'package:flutter/material.dart';

class TicTacToe extends StatefulWidget {
  const TicTacToe({Key? key}) : super(key: key);

  @override
  State<TicTacToe> createState() => _TicTacToeState();
}

class _TicTacToeState extends State<TicTacToe> {
  int scoreX = 0;
  int scoreO = 0;
  String turn = "X";

  List<String?> ans = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];
  List<List<int>> combination = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
  ];
  bool isDone = false;

  void playAgain(){
    setState(() {
      isDone = false;
      turn = "X";
      ans = [
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
      ];
    });
  }

  void answer(index) {
    if (ans[index] != "" || isDone) {
      return;
    }
    setState(() {
      ans[index] = turn;
    });
    checkWinner();
    setState(() {
      turn = turn == "X" ? "0" : "X";
    });
  }

  void checkWinner() {
    bool win = false;
    combination.forEach((element) {
      if (!win) {
        bool tmpWin = true;
        print(element);
        element.forEach((e) {
          if (ans[e - 1] != turn) {
            tmpWin = false;
          }
        });
        if(tmpWin){
          win = true;
        }
      }
    });

    if (win) {
      setState(() {
        isDone = true;
      });
      if(turn == "X"){
        setState(() {
          scoreX++;
        });
      }else{
        setState(() {
          scoreO++;
        });
      }
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Player ${turn == "X" ? "0" : "X"} Win!",textAlign: TextAlign.center,),
              actionsAlignment: MainAxisAlignment.center,
              actions: [
                ElevatedButton(onPressed: () {
                  playAgain();
                  Navigator.pop(context);
                }, child: Text("Play Again")),
                ElevatedButton(onPressed: () {
                  Navigator.of(context).pop();
                }, child: Text("Close")),
              ],
            );
          });
      return;
    }
    bool tmpDone = true;
    combination.forEach((element) {
      element.forEach((e) {
        if (ans[e - 1] == "") {
          tmpDone = false;
        }
      });
    });

    if (tmpDone) {
      setState(() {
        isDone = true;
      });
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("DRAW!",textAlign: TextAlign.center,),
              actionsAlignment: MainAxisAlignment.center,
              actions: [
                ElevatedButton(onPressed: () {
                  playAgain();
                  Navigator.pop(context);
                }, child: Text("Play Again")),
                // ElevatedButton(onPressed: (){}, child: Text("Close")),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Text(
                            "Player X",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                          Text(
                            "${scoreX}",
                            style: TextStyle(
                                color: Colors.orange,
                                fontWeight: FontWeight.bold,
                                fontSize: 30),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Text(
                            "Player O",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                          Text(
                            "${scoreO}",
                            style: TextStyle(
                                color: Colors.green,
                                fontWeight: FontWeight.bold,
                                fontSize: 30),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Text(
                "Player ${turn}'s turn",
                style: TextStyle(fontSize: 20, color: Colors.grey),
              ),
              GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: 9,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: 1,
                ),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      answer(index);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.3),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          borderRadius: BorderRadius.circular(10)),
                      margin: EdgeInsets.all(10),
                      child: ans.asMap().containsKey(index) && ans[index]! != ""
                          ? Center(
                              child: Text(
                                "${ans[index]}",
                                style: TextStyle(
                                    color: ans[index] == "X"
                                        ? Colors.orange
                                        : Colors.green,
                                    fontSize: 50),
                              ),
                            )
                          : Container(),
                    ),
                  );
                },
              ),
              Spacer(),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    scoreO = 0;
                    scoreX = 0;
                  });
                  playAgain();
                },
                child: Text(
                  "Reset Score",
                  style: TextStyle(fontSize: 16),
                ),
                style: ButtonStyle(
                  shape: MaterialStatePropertyAll(StadiumBorder()),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
