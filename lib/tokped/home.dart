import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class HomeTokped extends StatefulWidget {
  const HomeTokped({Key? key}) : super(key: key);

  @override
  State<HomeTokped> createState() => _HomeTokpedState();
}

class _HomeTokpedState extends State<HomeTokped> {
  Color primaryColor = Color(0xFF0f9e59);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          height: 36,
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: const TextField(
            decoration: InputDecoration(
              fillColor: Colors.white,
              hintText: "Cari di Tokopedia",
              icon: Icon(LineIcons.search),
              border: InputBorder.none,
            ),
          ),
        ),
        backgroundColor: primaryColor,
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 0),
            child: Stack(
              children: [
                IconButton(
                    onPressed: null,
                    icon: Icon(
                      LineIcons.envelope,
                      size: 30,
                      color: Colors.white,
                    )),
                Positioned(
                  child: Container(
                    width: 18,
                    height: 18,
                    decoration: BoxDecoration(
                        color: Colors.redAccent,
                        borderRadius: BorderRadius.circular(5)),
                    child: Center(
                      child: Text("9"),
                    ),
                  ),
                  right: 0,
                  top: 5,
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 0),
            child: Stack(
              children: [
                IconButton(
                    onPressed: null,
                    icon: Icon(
                      LineIcons.bell,
                      size: 30,
                      color: Colors.white,
                    )),
              ],
            ),
          ),
          Stack(
            children: [
              IconButton(
                  onPressed: null,
                  icon: Icon(
                    LineIcons.shoppingCart,
                    size: 30,
                    color: Colors.white,
                  )),
              Positioned(
                child: Container(
                  width: 18,
                  height: 18,
                  decoration: BoxDecoration(
                      color: Colors.redAccent,
                      borderRadius: BorderRadius.circular(5)),
                  child: Center(
                    child: Text("9"),
                  ),
                ),
                right: 0,
                top: 5,
              )
            ],
          ),
          Stack(
            children: [
              IconButton(
                  onPressed: null,
                  icon: Icon(
                    LineIcons.horizontalSliders,
                    size: 30,
                    color: Colors.white,
                  )),
              Positioned(
                child: CircleAvatar(
                  radius: 6,
                  backgroundColor: Colors.redAccent,
                ),
                right: 0,
                top: 5,
              )
            ],
          ),
          SizedBox(
            width: 10,
          )
        ],
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: RefreshIndicator(
        onRefresh: ()async{

        },
        child: ListView(
          padding: EdgeInsets.only(bottom: 100),
          children: [
            Container(
              decoration: BoxDecoration(color: primaryColor),
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.location_on_outlined,
                        color: Colors.white,
                      ),
                      Text(
                        " Dikirim ke",
                        style: TextStyle(color: Colors.white),
                      ),
                      Text(
                        " Rumah Firman Prayoga ",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      Icon(
                        CupertinoIcons.chevron_down,
                        size: 12,
                        color: Colors.white,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.all(10),
                    child: IntrinsicHeight(
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text(
                                  "Gopay",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Rp 12.000",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "100 Coins",
                                  style: TextStyle(color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                          VerticalDivider(),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text(
                                  "Reward",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Silver",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "8 Kupon Baru",
                                  style: TextStyle(color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                          VerticalDivider(),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text(
                                  "PLUS",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Kamu Terpilih",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "Langganan, Yuk!",
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GridView(
              padding: EdgeInsets.all(10),
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 6),
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Image.asset('assets/tokped/promo.png'),
                      ),
                      Text(
                        "Promo Hari Ini",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Image.asset('assets/tokped/semua.png'),
                      ),
                      Text(
                        "Lihat Semua",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/tokped/toserba.png',
                        height: 40,
                      ),
                      Text(
                        "Toserba",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/tokped/olahraga.png',
                        height: 40,
                      ),
                      Text(
                        "Olahraga",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Image.asset('assets/tokped/topup.png'),
                      ),
                      Text(
                        "Top-Up & Tagihan",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Image.asset('assets/tokped/travel.png'),
                      ),
                      Text(
                        "Travel & Entertain",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  for (var i = 0; i < 10; i++)
                    Container(
                      width: MediaQuery.of(context).size.width - 70,
                      height: 140,
                      margin: EdgeInsets.symmetric(horizontal: 5),
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(10)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          "https://picsum.photos/id/${i}/1200/500",
                          fit: BoxFit.cover,
                        ),
                      ),
                    )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GridView(
              padding: EdgeInsets.all(10),
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 6),
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/tokped/gofood.png',
                        height: 40,
                      ),
                      Text(
                        "GoFood",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/tokped/official.png',
                        height: 40,
                      ),
                      Text(
                        "Official Store",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/tokped/live.png',
                        height: 40,
                      ),
                      Text(
                        "Live Shopping",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/tokped/seru.png',
                        height: 40,
                      ),
                      Text(
                        "Tokopedia Seru",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/tokped/gopay.png',
                        height: 40,
                      ),
                      Text(
                        "GoPay Later Cicil",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/tokped/cod.png',
                        height: 40,
                      ),
                      Text(
                        "Bayar di Tempat",
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                "Lanjut cek ini, yuk",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  for (var i = 0; i < 10; i++)
                    Container(
                      width: 150,
                      height: 200,
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(.3),
                                blurRadius: 10,
                                spreadRadius: 1)
                          ]),
                      child: Column(
                        children: [
                          Expanded(
                            flex: 3,
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  topLeft: Radius.circular(10)),
                              child: Image.network(
                                "https://picsum.photos/id/${i+100}/1200/500",
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Cek ini juga, yuk!"),
                                Text("Alat Angkat Beban",style: TextStyle(
                                    fontWeight: FontWeight.bold
                                ),)
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Kejar Diskon Spesial",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Text("Berakhir dalam",style: TextStyle(
                          color: Colors.grey
                      ),),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.redAccent,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 5,vertical: 2),
                        child: Row(
                          children: [
                            Icon(LineIcons.clock,size: 15,color: Colors.white,),
                            SizedBox(
                              width: 5,
                            ),
                            Text("00:12:23",style: TextStyle(
                                color: Colors.white
                            ),),
                          ],
                        ),
                      ),
                      Spacer(),
                      Text("Lihat Semua",style: TextStyle(
                          color: primaryColor
                      ),),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  for (var i = 0; i < 10; i++)
                    Container(
                      width: 150,
                      height: 200,
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(.3),
                                blurRadius: 10,
                                spreadRadius: 1)
                          ]),
                      child: Column(
                        children: [
                          Expanded(
                            flex: 3,
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  topLeft: Radius.circular(10)),
                              child: Image.network(
                                "https://picsum.photos/id/${i+50}/1200/500",
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Cek ini juga, yuk!"),
                                Text("Alat Angkat Beban",style: TextStyle(
                                    fontWeight: FontWeight.bold
                                ),)
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        currentIndex: 0,
        selectedItemColor: primaryColor,
        unselectedItemColor: Colors.black54,
        type: BottomNavigationBarType.fixed,

        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home_filled,color: primaryColor,), label: "Home"),
          BottomNavigationBarItem(icon: Icon(LineIcons.medicalBook), label: "Feed"),
          BottomNavigationBarItem(icon: Icon(LineIcons.store), label: "Official Store"),
          BottomNavigationBarItem(icon: Icon(LineIcons.heart), label: "Wishlist"),
          BottomNavigationBarItem(icon: Icon(LineIcons.newspaper), label: "Transaksi"),
        ],
      ),
    );
  }
}
