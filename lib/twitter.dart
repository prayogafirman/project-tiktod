import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class Twitter extends StatefulWidget {
  const Twitter({Key? key}) : super(key: key);

  @override
  State<Twitter> createState() => _TwitterState();
}

class _TwitterState extends State<Twitter> with SingleTickerProviderStateMixin {
  late final TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              backgroundColor: Colors.white,
              title: Image.asset(
                'assets/twitter/icon.png',
                height: 50,
              ),
              pinned: true,
              floating: true,
              centerTitle: true,
              leading: Padding(
                padding: EdgeInsets.only(left: 5),
                child: IconButton(
                  icon: CircleAvatar(
                    radius: 30,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image.network("https://i.pravatar.cc/150?img=0"),
                    ),
                  ),
                  onPressed: null,
                ),
              ),
              forceElevated: innerBoxIsScrolled,
              elevation: 1,
              bottom: TabBar(
                labelStyle: TextStyle(
                    color: Colors.black87, fontWeight: FontWeight.bold),
                labelColor: Colors.black87,
                unselectedLabelColor: Colors.grey,
                indicatorColor: Colors.blue,
                indicatorSize: TabBarIndicatorSize.label,
                tabs: <Tab>[
                  Tab(text: 'For You'),
                  Tab(text: 'Following'),
                ],
                controller: _tabController,
              ),
            ),
          ];
        },
        body: TabBarView(
          controller: _tabController,
          children: <Widget>[
            forYou(),
            following(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: " "),
          BottomNavigationBarItem(icon: Icon(LineIcons.search), label: " "),
          BottomNavigationBarItem(icon: Icon(LineIcons.microphone), label: " "),
          BottomNavigationBarItem(icon: Icon(LineIcons.bell), label: " "),
          BottomNavigationBarItem(icon: Icon(LineIcons.envelope), label: " "),
        ],
      ),
    );
  }

  Widget forYou() {
    return RefreshIndicator(child: ListView.builder(
      padding: EdgeInsets.symmetric(vertical: 10),
      itemCount: 10,
      itemBuilder: (context, index) {
        return posting(id: index);
      },
    ), onRefresh: ()async{});
  }

  Widget following() {
    return RefreshIndicator(child: ListView.builder(
      padding: EdgeInsets.symmetric(vertical: 10),
      itemCount: 10,
      itemBuilder: (context, index) {
        return posting(id: index);
      },
    ), onRefresh: ()async{});
  }


  Widget posting({required int id}) {
    var rng = Random();
    if (rng.nextInt(100) & 2 == 0) {
      return onePost(id);
    } else if(rng.nextInt(100) & 2 == 1){
      return textPost(id);
    }else{
      return multiplePost(id);
    }
  }

  Widget onePost(id) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          // decoration: BoxDecoration(
          // ),
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 23,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Image.network("https://i.pravatar.cc/150?img=${id}"),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Firman Prayoga ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          "@loremipsum",
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.network(
                            "https://picsum.photos/id/${id}/500/500",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  Icons.bar_chart,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 1.5M",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  CupertinoIcons.chat_bubble,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 1.487",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  CupertinoIcons.repeat,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 1.238",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  LineIcons.heart,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 9.889",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Icon(
                              LineIcons.upload,
                              color: Colors.grey,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Divider()
      ],
    );
  }

  Widget textPost(id) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          // decoration: BoxDecoration(
          // ),
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 23,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Image.network("https://i.pravatar.cc/150?img=${id}"),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Firman Prayoga ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          "@loremipsum",
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    // Padding(
                    //   padding: EdgeInsets.all(10),
                    //   child: GridView.builder(
                    //     physics: NeverScrollableScrollPhysics(),
                    //     shrinkWrap: true,
                    //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    //         crossAxisCount: 4),
                    //     itemBuilder: (context, index) {
                    //       return Container(
                    //         color: Colors.redAccent,
                    //       );
                    //     },
                    //   ),
                    // ),
                    Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  Icons.bar_chart,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 1.5M",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  CupertinoIcons.chat_bubble,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 1.487",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  CupertinoIcons.repeat,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 1.238",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  LineIcons.heart,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 9.889",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Icon(
                              LineIcons.upload,
                              color: Colors.grey,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Divider()
      ],
    );
  }

  Widget multiplePost(id) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          // decoration: BoxDecoration(
          // ),
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 23,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Image.network("https://i.pravatar.cc/150?img=${id}"),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Firman Prayoga ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          "@loremipsum",
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: Column(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey,
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.network("https://picsum.photos/id/${id}/500/500",fit: BoxFit.cover,),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey,
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.network("https://picsum.photos/id/${id}/500/500",fit: BoxFit.cover,),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Expanded(
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey,
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.network("https://picsum.photos/id/${id}/500/500",fit: BoxFit.cover,),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey,
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.network("https://picsum.photos/id/${id}/500/500",fit: BoxFit.cover,),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  Icons.bar_chart,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 1.5M",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  CupertinoIcons.chat_bubble,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 1.487",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  CupertinoIcons.repeat,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 1.238",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Row(
                              children: [
                                Icon(
                                  LineIcons.heart,
                                  color: Colors.grey,
                                ),
                                Text(
                                  " 9.889",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            child: Icon(
                              LineIcons.upload,
                              color: Colors.grey,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Divider()
      ],
    );
  }
}
