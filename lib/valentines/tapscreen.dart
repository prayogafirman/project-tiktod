import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:onboarding_animation/onboarding_animation.dart';

class TapScreen extends StatefulWidget {
  const TapScreen({Key? key}) : super(key: key);

  @override
  State<TapScreen> createState() => _TapScreenState();
}

class _TapScreenState extends State<TapScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            Colors.white,
            Colors.pinkAccent.withOpacity(.3)],
          begin: Alignment.topCenter,
              end: Alignment.bottomCenter
        )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Lottie.network(
              "https://assets1.lottiefiles.com/packages/lf20_YEZz8Y.json",
            ),
            InkWell(
              child: Hero(
                tag: "1",
                child: Material(
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.pink,
                        borderRadius: BorderRadius.circular(100),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.white,
                            offset: Offset(0.0, 1.0), //(x,y)
                            blurRadius: 6.0,
                          ),
                        ]
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                    child: Text("Tap to see the magic",style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16
                    ),),
                  ),
                ),
              ),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SliderPage(),));
              },
            )
          ],
        ),
      ),
    );
  }
}

class SliderPage extends StatefulWidget {
  const SliderPage({Key? key}) : super(key: key);

  @override
  State<SliderPage> createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  bool isShow = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(milliseconds: 100),() {
      setState(() {
        isShow = true;
      });
    },);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.transparent,
      body: Hero(
        tag: '1',
        child: Material(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                color: Colors.pinkAccent
            ),
            child: OnBoardingAnimation(
              pages: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.all(20),
                  child: contentCard(
                      img: "assets/woman.png",
                      title: "When I First Saw You",
                      desc: "The moment I first saw you, my heart skipped a beat. You captured my attention with your beauty and grace, and I knew you were someone special."
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.all(20),
                  child: contentCard(
                      img: "assets/motoran.png",
                      title: "All Time We Spent",
                      desc: "The time we've spent together is priceless to me. Each moment we've shared, whether it's the big adventures or the quiet moments of everyday life, has brought us closer and filled my heart with joy. "
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.all(20),
                  child: contentCard(
                      img: "assets/nikah.png",
                      title: "Let's Grow Old Together",
                      desc: "Let's grow old together, sharing life's ups and downs. With you by my side, the future is bright."
                  ),
                ),
              ],
              indicatorDotHeight: 7.0,
              indicatorDotWidth: 7.0,
              indicatorType: IndicatorType.expandingDots,
              indicatorPosition: IndicatorPosition.bottomCenter,
              indicatorActiveDotColor: Colors.white,
              indicatorInActiveDotColor: Colors.white.withOpacity(.4),
            ),
          ),
        ),
      ),
    );
  }

  Widget contentCard({String? img,String? title, String? desc}) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 2), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(40)
      ),
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          AspectRatio(aspectRatio: 1,child: Image.asset("${img}"),),
          Spacer(),
          Text("${title}",style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 22
          ),),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text("${desc}",textAlign: TextAlign.center,),
          ),
          if(isShow)
          Container(
            height: 200,
            child: Lottie.network("https://assets1.lottiefiles.com/packages/lf20_9ndsxmqq.json"),
          )
        ],
      ),
    );
  }
}

